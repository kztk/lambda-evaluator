all :
	hastec Eval.hs

opt: Eval_pre.js
	closure-compiler -O SIMPLE --js Eval_pre.js --js_output_file Eval.js

Eval_pre.js :
	# hastec --opt-all Eval.hs
	hastec --opt-whole-program Eval.hs -o Eval_pre.js

clean :
	rm -f *.o
	rm -f *.hi
	rm -f Parser.hs
	rm -f Lexer.hs
	rm -f Eval.js
	rm -f Eval_pre.js
