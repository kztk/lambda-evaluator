module Main where

import Syntax
import Data.IORef
import Control.Monad (when, (=<<), void)
import Control.Monad.Fix (mfix)
import Control.Monad.IO.Class  (MonadIO(..))

import Data.Char (isSpace)

import Haste.Foreign
import Haste
import Haste.DOM
import Haste.Events
import Haste.Prim
import Haste.Performance

getCurrentTime =
  navigationStart

main :: IO ()
main = do
  refDStr <- newIORef ""
  refEStr  <- newIORef ""

  changeFlag <- newIORef True

  refPrevTime <- newIORef =<< getCurrentTime
  let checkDifferent =
       withElems ["decls", "exp"] $ \[decls, exp] -> do
        Just currDStr <- getValue decls
        Just currEStr <- getValue exp
        prevDStr <- readIORef refDStr
        prevEStr <- readIORef refEStr
        writeIORef refDStr currDStr
        writeIORef refEStr currEStr
        return ( (prevDStr /= currDStr) || (prevEStr /= currEStr) )



  let checkParseErrorPeriodically = do
--          prevTime <- readIORef refPrevTime
--          currTime <- getCurrentTime
          ch <- readIORef changeFlag
          if ch then do
            b <- checkDifferent
            when b $ checkParseErrors (\_ _ -> return ())
          else
            writeIORef changeFlag False
          void $ setTimer (Once 200) checkParseErrorPeriodically


  withElem "decls" $
    \decls -> do
      onEvent decls Change $ \_ -> do
        writeIORef changeFlag True
        checkParseErrors (\_ _ -> return ())
      -- onEvent decls KeyUp  $ \_ -> writeIORef refPrevTime =<< getCurrentTime

  r <- newIORef False
  stopRunRRef <- (newIORef r :: IO (IORef (IORef Bool)))

  let startEval = do
        prevRef <- readIORef stopRunRRef
        writeIORef prevRef True
        stopRun <- newIORef False
        writeIORef stopRunRRef stopRun
        evaluateLambda stopRun

  withElem "exp" $
    \exp -> do
      onEvent exp Change   $ \_ -> do
       writeIORef changeFlag True
       checkParseErrors (\_ _ -> return ())
      onEvent exp KeyPress $ \keyEvent ->
          when (keyCode keyEvent == 13) startEval -- Enter key

  checkParseErrorPeriodically


  withElem "eval" $ \eval -> do
    onEvent eval Click $ \_ -> startEval

  return ()

checkParseErrors ::  MonadIO m => ([(Name, Exp Info)] -> Exp Info -> m ()) -> m ()
checkParseErrors cont =
  withElems ["decls", "exp"] $ \[decls, exp] -> do
    Just dStr <- getValue decls
    Just eStr <- getValue exp
    case (parseDecls dStr, parseExp eStr) of
     (Right d, Right e) -> do
       withElem "eval" $ enableButton
       clearParseErrors >> cont d e
     (x, y) -> do
       withElem "eval" $ disableButton
       clearParseErrors
       either (\l -> printParseErrorId "Parse Error in Decls" "d_error" (show l)) (const $ printParseOK "d_error") x
       when ( dropWhile isSpace eStr /= "" ) $
           either (\l -> printParseErrorId "Parse Error in Exp" "e_error" (show l)) (const $ printParseOK "e_error") y


printParseOK :: MonadIO m => ElemID -> m ()
printParseOK id = return ()

clearParseErrors :: MonadIO m => m ()
clearParseErrors = do
  withElem "d_error" $ \s -> clearChildren s
  withElem "e_error" $ \s -> clearChildren s

printParseErrorId :: MonadIO m => String -> ElemID -> String -> m ()
printParseErrorId title id t =
  withElem id $ \s -> do
    clearChildren s
    d <- newElem "div" `with` [ attr "class" =: "error_balloon" ]
    h2  <- newElem "h2"
    appendChild h2 =<< newTextElem title
    appendChild d h2
    pre <- newElem "pre" `with` [ attr "class" =: "error" ]
    appendChild pre =<< newTextElem t
    appendChild d pre
    appendChild s d


removeAttr :: MonadIO m => Elem -> String -> m ()
removeAttr elem attr = liftIO $
   (ffi (toJSStr "(function (e,a) { e.removeAttribute(a); })") :: Elem -> JSString -> IO ()) elem (toJSStr attr)

disableButton :: MonadIO m => Elem -> m ()
disableButton elem =
  setAttr elem "disabled" "disabled"

enableButton :: MonadIO m => Elem -> m ()
enableButton elem =
  removeAttr elem "disabled"

waitTime :: Int
waitTime = 30

createResultPane :: MonadIO m => m Elem
createResultPane = do
  Just result <- elemById "result"
  clearChildren result
  res <- newElem "div"
  appendChild result res
  return res

createStopButton :: MonadIO m => m Elem
createStopButton = do
  Just stopHolder <- elemById "stop_holder"
  clearChildren stopHolder
  stopButton <- newElem "input" `with` [ attr "type" =: "button", attr "id" =: "stop", attr "value" =: "Stop!" ]
  appendChild stopHolder stopButton
  return stopButton

evaluateLambda :: IORef Bool -> IO ()
evaluateLambda stopRef = do
  checkParseErrors $ \decls exp -> do
    res <- createResultPane
    let exp' = resolveDecls (decls) exp
    ref <- newIORef (normalize' exp')

    stopButton <- createStopButton

    contRef  <- mfix $ \contRef -> newIORef $ do
                  setProp stopButton "value" "Stop!"
                  b <- readIORef stopRef
                  when (not b) $ void $ setTimer (Once waitTime) $ printResults res stopButton ref contRef
    contRef_ <- newIORef $ setProp stopButton "value" "Resume!"

    void $ onEvent stopButton Click $ \_ -> do
      k  <- readIORef contRef
      k_ <- readIORef contRef_
      writeIORef contRef  k_
      writeIORef contRef_ k
      k_

    k <- readIORef contRef
    k
  where

    printResults :: Elem -> Elem -> IORef [Dec Info] -> IORef (IO ()) -> IO ()
    printResults res stopButton ref contRef = do
      ns <- readIORef ref
      case ns of
       [Normal n] -> do
         appendChild res =<< printNormal n
         setProp stopButton "value" "Done"
         disableButton stopButton
       (Redex red k:ns) -> do
         writeIORef ref ns
         appendChild res =<< printRedex red k
         k <- readIORef contRef
         k

    lineWith :: (Elem -> IO ()) -> IO Elem
    lineWith k = do
      d  <- newElem "div" `with` [ attr "class" =: "line" ]
      -- ln <- newElem "span" `with` [ attr "class" =: "ln" ]
      -- appendChild d ln
      d0 <- newElem "div" `with` [ attr "class" =: "line_body" ]
      appendChild d d0
      k d0
      return d


    printNormal :: Exp Info -> IO Elem
    printNormal e    = do
      lineWith $ \d ->
         printWith d 0 (\_ -> newTextElem "[_]") e

    printRedex :: Exp Info -> (Exp Info -> Exp Info) -> IO Elem
    printRedex red k = do
      -- n  <- printWith 0 (\_ -> newTextElem "[_]") red
      -- n' <- newElem "span" `with` [ attr "class" =: "redex" ]
      -- appendChild n' n
      lineWith $ \d ->
        printWith d 0 (\b -> do n <- newElem "span" `with` [attr "class" =: "redex"]
                                printWithR n b red
                                return n) (k (var HOLE))

    printWithR n b (EApp i (EAbs j x e1) e2) =
      withParenIf (b >= 1) n $ \n0 -> do
        f  <- newElem "span" `with` [ attr "class" =: "func" ]
        a  <- newElem "span" `with` [ attr "class" =: "arg" ]
        let k = (\_ -> newTextElem "[_]")
        printWith f 1 k (EAbs (j { special = Nothing }) x e1)
        printWith a 2 k e2
        appendChild n0 f
        appendChild n0 =<< newTextElem " "
        appendChild n0 a

    printWithR n b e =
      printWith n b (\_ -> newTextElem "[_]") e

    withParenIf b n k = do
      n0 <- parenElem b n
      when b $ do
        left <- newElem "span" `with` [ attr "class" =: "lp" ]
        appendChild left =<< newTextElem "("
        appendChild n0 left
      k n0
      when b $ do
        right <- newElem "span" `with` [ attr "class" =: "rp" ]
        appendChild right =<< newTextElem ")"
        appendChild n0 right


    parenElem b n =
      if b then do n0 <- newElem "span" `with` [ attr "class" =: "paren" ]
                   appendChild n n0
                   assignHovering n0
                   return n0
      else
        return n


    printWith n b k e = go b n e
      where
        go b n (EVar _ HOLE)  = appendChild n =<< k b
        go b n (EVar _ x)     = appendChild n =<< newTextElem (show x)
        go b n (EApp _ e1 e2) =
          withParenIf (b >= 2) n $ \n0 -> do
            -- when (b >= 2) $ appendChild n0 =<< newTextElem "("
            go 1 n0 e1
            appendChild n0 =<< newTextElem " "
            go 2 n0 e2
            --when (b >= 2) $ appendChild n0 =<< newTextElem ")"
        go b n (EAbs i x e1) =
          case special i of
           Just c ->
             appendChild n =<< newTextElem (show c)
           Nothing -> do
             withParenIf (b >= 1) n $ \n0 -> do
               -- when (b >= 1) $ appendChild n0 =<< newTextElem "("
               appendChild n0 =<< newTextElem "λ"
               appendChild n0 =<< newTextElem (show x)
               appendChild n0 =<< newTextElem "."
               go 0 n0 e1
               -- when (b >= 1) $ appendChild n0 =<< newTextElem ")"

assignHovering n = do
   onEvent n MouseOver $ \_ -> do
      a <- ancestorOfClass 3 n "paren"
      case a of
           Just p ->  toggleClass p "hovering"
           Nothing -> return ()
      toggleClass n "hovering"
   onEvent n MouseOut $ \_ -> do
      a <- ancestorOfClass 3 n "paren"
      case a of
        Just p -> toggleClass p "hovering"
        Nothing -> return ()
      toggleClass n "hovering"


ancestorOfClass :: Int -> Elem -> String -> IO (Maybe Elem)
ancestorOfClass 0 n c = return Nothing
ancestorOfClass k n c =
  do p <- parent n
     case p of
      Nothing ->
        return Nothing
      Just x -> do
        b <- hasClass x c
        if b then
          return $ Just x
        else
          ancestorOfClass (k-1) x c

parent :: Elem -> IO (Maybe Elem)
parent =
  ffi (toJSStr "(function (e) {var p = e.parentNode; if (p != null && (p instanceof HTMLElement)) {return p;} else {return null;} })") :: Elem -> IO (Maybe Elem)
