{-# LANGUAGE FlexibleInstances, FlexibleContexts #-}

module Syntax where

import qualified Data.Set as S
import Debug.Trace
import Control.Applicative
import Data.Char


-- import qualified Text.ParserCombinators.ReadP as P
-- import Text.ParserCombinators.ReadP ((+++), (<++))

import qualified Text.Parsec as P

data Name = Name  String
          | HOLE
          | IName Int -- used internally
          deriving (Eq, Ord)

instance Show Name where
  show (Name n) = n
  show HOLE     = "[_]"
  show (IName i) = "_x" ++ show i

data Special = CNum   Int
             | CBool  Bool
             | CDef   Name

instance Show Special where
  show (CNum i)  = show i
  show (CBool b) = show b
  show (CDef  n) = show n

data Exp i = EVar i Name
           | EAbs i Name (Exp i)
           | EApp i (Exp i) (Exp i)


instance Show (Exp Info) where
  show = go 0
    where
      parenWhen b s = if b then "(" ++ s ++ ")" else s

      go _ (EVar _ x)   = show x
      go b (EAbs i x e) =
        case special i of
         Nothing ->
           parenWhen (b >= 1) ("\\" ++ show x ++ "." ++ go 0 e)
         Just  c ->
           show c
      go b (EApp i e1 e2) =
        parenWhen (b >= 2) (go 1 e1 ++ " " ++ go 2 e2)


info (EVar i _) = i
info (EAbs i _ _) = i
info (EApp i _ _) = i

data Info = Info { isNormal :: Bool, isWHNF :: Bool, isNeutral :: Bool, special :: Maybe Special, fvi :: S.Set Name  }

var x   = EVar i x
  where i = Info True True True Nothing (S.singleton x)

lam x e = lamS x e Nothing
lamS x e s = EAbs i x e
  where i = Info (isNormal (info e)) True False s (S.delete x (fvi (info e)))

app e1 e2 = EApp i e1 e2
  where
    i = Info b isNeutral1 b Nothing (S.union (fvi $ info e1) (fvi $ info e2))
    isNeutral1 = isNeutral (info e1)
    b = isNeutral1 && isNormal (info e2)


data Dec i = Normal (Exp i)
           | Redex  (Exp i) (Exp i -> Exp i)


instance Show (Dec Info) where
  show (Normal e)     = show e
  show (Redex  red f) = "<" ++ show red ++ ", " ++ show (f hole) ++ ">"
    where
      hole = var HOLE

findRedexNo (EVar i x) = Normal (EVar i x)
findRedexNo (EAbs i x e)
  | isNormal i = Normal (EAbs i x e)
  | otherwise =
      case findRedexNo e of
       Normal e -> Normal (lamS x e (special i))
       Redex red k ->
         Redex red ((lam x) . k)
findRedexNo (EApp i e1 e2)
  | isNormal i = Normal (app e1 e2)
  | otherwise =
      case findRedexBN e1 of
       Normal (EAbs j x e) ->
         Redex (EApp i (EAbs j x e) e2) id
       Normal e1' ->
         case findRedexNo e1' of
          Normal e1'' ->
            case findRedexNo e2 of
             Normal e2' ->
               Normal (app e1'' e2')
             Redex red k ->
               Redex red (app e1' . k)
          Redex red k ->
            Redex red ((\x -> app x e2) . k)
       Redex red k ->
         Redex red ((\x -> app x e2) . k)

findRedexBN (EVar i x)     = Normal (EVar i x)
findRedexBN (EAbs i x e)   = Normal (EAbs i x e)
findRedexBN (EApp i e1 e2)
  | isNormal i = Normal (EApp i e1 e2)
  | otherwise  =
      case findRedexBN e1 of
       Normal e1' -> Normal (app e1' e2)
       Redex red k -> Redex red ((\x -> app x e2) . k)


reduce :: Exp Info -> Exp Info
reduce (EApp _ (EAbs _ x e1) e2) =
  -- trace ("<" ++ show e1 ++ " " ++ show (fv e1) ++ ">") $
  --  (\s -> trace ("<<" ++ "(" ++ show e1 ++ ")" ++ show (alpha (fv e2) e1) ++ "[" ++ show e2 ++ "/" ++ show x ++ "]" ++ " ==> " ++ show s ++ ">>\n") s) $
  substitute x e2 (alpha (fv e2) e1)

{- |
Evaluation is implemented by iteration of one-step reductions.
This implementation is not fast, but don't worry.
We will print every intermediate reduction result, and DOM operation is
much more slower;)
-}

oneStep :: Exp Info -> Either (Exp Info) (Exp Info)
oneStep e =
  case findRedexNo e of
   Normal e'    -> Left e'
   Redex  red k -> -- trace ("<O"++ show e ++ " --> " ++show (Redex red k)++">") $
                   Right (k (reduce red))

normalize :: Exp Info -> [Exp Info]
normalize e =
  case oneStep e of
   Left  e'  -> [e]
   Right e'  -> e:normalize e'

normalize' :: Exp Info -> [Dec Info]
normalize' e =
  case findRedexNo e of
   Normal e'   -> [Normal e']
   Redex red k ->
     Redex red k:normalize' (k (reduce red))



churchNumeral :: Int -> Exp Info
churchNumeral n = lamS (Name "s") (lam (Name "z") (go n)) (Just (CNum n))
  where
    go 0 = var (Name "z")
    go n = app (var (Name "s")) (go (n-1))

churchBool :: Bool -> Exp Info
churchBool b = lamS (Name "t") (lam (Name "f") (var tf)) (Just (CBool b))
  where
    tf = if b then Name "t" else Name "f"


substitute :: Name -> Exp Info -> Exp Info -> Exp Info
substitute x e exp
  | not (x `S.member` fv exp) = exp
substitute x e (EVar i y)
  | x == y    = e
  | otherwise = EVar i y
substitute x e (EAbs i y e')
  | x == y    = EAbs i y e'
  | otherwise = lamS y (substitute x e e') (special i) -- special ones are closed
substitute x e (EApp _ e1 e2)
  = app (substitute x e e1) (substitute x e e2)

fv :: Exp Info -> S.Set Name
fv = fvi . info
-- fv :: Exp i -> S.Set Name
-- fv (EVar _ n)     = S.singleton n
-- fv (EAbs _ n e)   = S.delete n (fv e)
-- fv (EApp _ e1 e2) = S.union (fv e1) (fv e2)


-- This implementation is slow but allow us to reuse
-- existing short and fancy names as possible
alpha :: S.Set Name -> Exp Info -> Exp Info
alpha existingNames e =
  -- (\s -> trace ("<" ++ show e ++ " " ++ show existingNames ++ " ---> " ++ show s ++ ">") s) $
  go [] existingNames e
  where
    pick es (n:ns) | n `S.member` es = pick es ns
                   | otherwise       = n

    go ls ns (EVar i x) = var $ maybe x id (lookup x ls)
    go ls ns (EAbs i x e)
      | x `S.member` ns =
          let n = pick (S.union ns (fv e)) newNames
          in lamS n (go ((x,n):ls) (S.insert n ns) e) (special i)
      | otherwise =
          lamS x (go ls (S.insert x ns) e) (special i)
    go ls ns (EApp i e1 e2)  = app (go ls ns e1) (go ls ns e2)

newNames = [ Name [c] | c <- "xyzwstupqabcdefghkmnrov" {- no l,i,j -} ] ++ [ IName i | i <- [1..]]


resolveDecls :: [(Name, Exp Info)] -> Exp Info -> Exp Info
resolveDecls defs exp =
  foldr (\(n,e') e -> let e'' = addSpec e' n
                      in substitute n e'' (alpha (fv e'') e)) exp defs
  where
    addSpec (EVar i x)     n = EVar (i { special = Just $ CDef n }) x
    addSpec (EApp i e1 e2) n = EApp (i { special = Just $ CDef n }) e1 e2
    addSpec (EAbs i x e)   n = EAbs (i { special = Just $ CDef n }) x e


{-

Exp -> \x -> Exp
    -> AppExp

AppExp -> AppExp'
AppExp' -> AExp AppExp'
        -> AExp

AExp -> ( Exp )
     -> ...



-}


-- For Debugging and Performance Evaluation

infixl 9 @@
(@@) = app

mPred = either (error . show) id $
           parseExp "\\n s z. n (\\p. p (\\x y k. k y (s y))) (\\k. k z z) (\\x.\\y.x)"

instance Num (Exp Info) where
  e1 + e2 = lam s $ lam z $ e1 @@ var s @@ (e2 @@ var s @@ var z)
      where
        s = Name "s"
        z = Name "z"
  e1 - e2 = e2 @@ mPred @@ e1
  e1 * e2 = lam s $ lam z $ e1 @@ (e2 @@ var s) @@ var z
      where
        s = Name "s"
        z = Name "z"
  negate  = undefined
  abs     = id
  signum  = id
  fromInteger = churchNumeral . fromIntegral


-- Parsing

skipSpaces :: P.Stream s m Char => P.ParsecT s u m ()
skipSpaces = P.spaces

munch q = P.many (P.satisfy q)
munch1 q = P.many1 (P.satisfy q)


parseExp :: String -> Either P.ParseError (Exp Info)
parseExp s = P.parse (pExp <* P.eof) "" s
  -- foldr (\x _ -> Just x) Nothing $
  --             do (a,"") <- P.readP_to_S pExp s
  --                return a

parseDecls :: String -> Either P.ParseError [(Name, Exp Info)]
parseDecls s = P.parse pDecls "" s

pDecls = P.many pDecl

pDecl = withSpaces
         ((,) <$> pVar <* withSpaces (P.string "=") <*> pExp <* P.string ".")

-- pAExp :: P.ReadP (Exp Info)
pAExp = pNum <|> (var <$> pVar) <|> parenP pExp

-- pExp :: P.ReadP (Exp Info)
pExp = withSpaces pAbs

parenP p = P.string "(" *> p <* P.string ")"
withSpaces p = skipSpaces *> p <* skipSpaces

parenPWhen False p =
  skipSpaces *> p <* skipSpaces
parenPWhen True  p =
  skipSpaces *> parenP p <* skipSpaces

-- pNum :: P.ReadP (Exp Info)
pNum = a2i <$> munch1 (`elem` ['0'..'9'])
  where
    a2i = churchNumeral . foldl (\n c -> 10 * n + (ord c - ord '0')) 0

-- pVar :: P.ReadP Name
pVar = fmap Name $ (:) <$>
         P.satisfy isAlpha <*> munch (\x -> isAlphaNum x || x == '_' || x == '\'')

-- pAbs :: P.ReadP (Exp Info)
pAbs = P.try ((\ns e -> foldr lam e ns)<$>
         (pLambda *> pVars) <*>
          (pArrow *> skipSpaces *> pExp))
       <|> pApp
  where
    pVars   = P.many1 (withSpaces pVar)
    pLambda = P.string "\\" <|> P.string "λ" <|> P.string "fun"
    pArrow  = P.string "." <|> P.string "->"

-- pApp :: P.ReadP (Exp Info)
pApp = (foldl1 app <$> P.many1 (withSpaces pAExp))

-- manyGready1 p = (:) <$> p <*> (manyGready p)
-- manyGready p = manyGready1 p <|> return []



-- data Frame = FArg Exp Env Mode -- _ Exp
--            | FApp Exp Env      -- Exp -   -- where Exp is neutral
--            | FAbs Name         -- \x. -
--            deriving Show

-- type Stack = [Frame]



-- data Config = Config Mode Stack Env Exp Status

-- instance Show Config where
--   show (Config m s env e t) =
--     "[" ++ modeName m ++ "-" ++ showStatus t ":" ++ show s ++ " | " ++ show env ++ " | " ++ show exp ++ "]"
--     where
--       modeName CallByName  = "BN"
--       modeName NormalOrder = "NO"
--       showStatus Thunk   = "D"
--       showStatus Neutral = "U"


-- data Mode   = CallByName | NormalOrder
-- data Status = Thunk | Neutral

-- newtype Env = Env [(Name, (Exp, Env))]
--             deriving Show

-- lookupEnv :: Name -> Env -> Maybe (Exp, Env)
-- lookupEnv x (Env ls) = lookup x ls

-- addEnv :: Name -> (Exp, Env) -> Env -> Env
-- addEnv x v (Env ls) = Env $ (x,v):ls


-- step :: Config -> (Config, Bool)
-- step (Config mode stack env exp Thunk) =
--   case exp of
--    EVar x ->
--      case lookupEnv x env of
--       Nothing ->
--         (Config mode stack env exp Neutral, False)
--       Just (exp', env') ->
--         step (Config mode stack env' exp' Thunk)
--    EAbs x e i ->
--      case mode of
--       CallByName ->
--         case stack of
--          FArg e' env' m:stack' ->
--            (Config m stack' (addEnv x (e',env') env) e Thunk, True)
--          _ ->
--            error "This case shouldn't happen"
--       NormalOrder ->
--         (Config mode (FAbs x:stack) env e Thunk, False)
--    EApp e1 e2 ->
--      (Config CallByName (FArg e2 env mode:stack) env e1 Thunk, False)
-- step (Config mode stack env exp Neutral) =
--   case stack of
--    FArg exp' env' m:stack' ->
--      case m of
--       CallByName ->
--         (Config m stack' env (EApp exp exp') Neutral, False)
--       NormalOrder ->
--         (Config m (FApp exp env:stack') env' exp' Thunk, False)
--    FApp exp' env':stack' ->
--      -- mode must be NormalOrder for this case
--      (Config mode stack' env (EApp exp' exp) Neutral, False)
--    FAbs x:stack' ->
--      (Config mode stack' env (EAbs x exp Nothing) Neutral, False)
